// Write a program to calculate the total price of your phone purchase.
// You will keep purchasing phones (hint: loop!) until you
// run out of money in your bank account.
// You’ll also buy accessories
// for each phone as long as your purchase amount is below your mental spending threshold.

// • After you’ve calculated your purchase amount, add in the tax,
// then print out the calculated purchase amount, properly formatted.

// • Finally, check the amount against your bank account balance to
// see if you can afford it or not.

// • You should set up some constants for the “tax rate,” “phone
// price,” “accessory price,” and “spending threshold,” as well as a
// variable for your “bank account balance.”

// • You should define functions for calculating the tax and for formatting
// the price with a “$” and rounding to two decimal
// places. (All can be done with stactic values)

// Bonus Challenge: Try to incorporate input into this program, perhaps with the prompt(..) covered in Input. You may prompt the user for their bank account balance, for example. Have fun and be creative!


//-----------------------------------------------------------------------------------------

const nTaxRate = 18;
const nPhonePrice = 1000;
const nAccessoryPrice = 100;

var nBankBalance = prompt("Enter Bank Balance: ");

//Just for Example we set Spending Threshold as half of bank balnce
const nSpendingThreshold = nBankBalance / 2;

var nAmount = 0;
var nPhone = 0;
var nAccessory = 0;

var nTotalPhonePurchase = (nPhonePrice + getTax(nPhonePrice));
var nTotalAccessoryPurchase = (nAccessoryPrice + getTax(nAccessoryPrice));

// function for calculating tax for each item
function getTax(nTax) {
    return nTax * nTaxRate / 100;
}

// here nPhonePrice take because to check in last loop 
// is my balance is enough for buy last phone
while ((nAmount + nPhonePrice) < nBankBalance) {

    nAmount = nAmount + nTotalPhonePurchase;

    //this is checking for to buying Accessory 
    // I am able to buy Accessory under my Spending Threshold
    if (nAmount < nSpendingThreshold) {
        nAmount = nAmount + nTotalAccessoryPurchase;
        //to calculate how much i buy Accessory
        nAccessory++;
    }
    //to calculate how much i buy Phone
    nPhone++;
}


document.getElementById("totalbill").innerHTML = "$" + nAmount.toFixed(2)
document.getElementById("tpp").innerHTML = nPhone
document.getElementById("tap").innerHTML = nAccessory
document.getElementById("ab").innerHTML = "$" + (nBankBalance - nAmount)

console.log("Total bill: $" + nAmount.toFixed(2));
console.log("Total phone Purchase: " + nPhone);
console.log("Total Accessory Purchase: " + nAccessory);
console.log("Avilable Balance: $" + (nBankBalance - nAmount));

if (nAmount > nBankBalance) {
    console.log("-You can't afford this bill-");
}